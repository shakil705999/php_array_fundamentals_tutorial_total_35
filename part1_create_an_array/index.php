 <?php include'header.php'; ?>
			
				<section class="maincontent">
				<hr/>
				PHP Array Function - (Create an Array)
				<hr/>
				
				
					<?php
						/*
							//indexed array..................................
							
							$car=array("volvo","toyota","bmw");
							//echo $car[2];
							$length=count($car);
							//echo $length;
							for($i=0;$i<$length;$i++){
								echo $car[$i]."<br>";
						*/
							
					
							//associated array.............................................
					
						/*
							$age=array(
								"abdullah"=>"80",
								"noman"=>"70",
								"asik"=>"60"
								
							);
							foreach($age as $key=>$value){
								echo "Name=".$key.", age=".$value;
								echo "<br>";
							}
						*/
						
						
							//multi dimensional array...........................................
							
							$car=array(
								array("volvo","100","20"),
								array("Lamborgini","200","30"),
								array("Toyota","300","40")
							);
							echo $car[1][0];
						
					?>
				</section>
<?php include'footer.php'; ?>
				
			
	</body>
</html>